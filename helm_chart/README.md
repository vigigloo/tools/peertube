# Peertube [Helm chart]

PeerTube is a free and open-source, decentralized, federated video platform powered by ActivityPub and WebTorrent, that uses peer-to-peer technology to reduce load on individual servers when viewing videos.

* Decentralization means that videos are hosted by a network of computers, rather than a single company's servers. This means that no one party controls access to the content. It allows for a more democratic web, where anyone can publish and view content.
* Federation is a way of connecting different software platforms that allows them to communicate with each other. In PeerTube's case, it means that different instances, hosted by different individuals or organizations, can share videos with each other. This opens up a wide range of content to viewers, and allows creators to reach audiences on other instances.
* Peer-to-peer is a model where each user of the network provides and consumes resources. This is opposed to the centralized model, where a server provides resources to clients. In the case of PeerTube, it means that when watching a video, you're also helping to provide that video to other viewers. This reduces load on the server, and can potentially improve performance.
* ActivityPub is a protocol for building decentralized social networking services. It enables communication between server and client, as well as server-to-server. WebTorrent is a streaming torrent client for the web browser, which allows you to download files from people nearby. These technologies together make PeerTube powerful and versatile.
