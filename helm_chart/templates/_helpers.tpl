{{/* vim: set filetype=mustache: */}}

{{- define "peertube.commonEnv" -}}
- name: PEERTUBE_WEBSERVER_HOSTNAME
  value: {{ .Values.hostname | quote }}
- name: PEERTUBE_SECRET
  value: {{ .Values.secret | default (randAlphaNum 32) | quote }}
{{- if .Values.adminEmail }}
- name: PEERTUBE_ADMIN_EMAIL
  value: {{.Values.adminEmail | quote}}
{{- end -}}

{{- if .Values.postgresql.host }}
- name: PEERTUBE_DB_HOSTNAME
  value: {{.Values.postgresql.host | quote }}
- name: PEERTUBE_DB_NAME
  value: {{.Values.postgresql.name | quote }}
- name: PEERTUBE_DB_USERNAME
  value: {{.Values.postgresql.username | quote }}
- name: PEERTUBE_DB_PASSWORD
  value: {{.Values.postgresql.password | quote }}
- name: PEERTUBE_DB_SSL
  value: {{.Values.postgresql.ssl | quote }}
{{- end -}}

{{- if .Values.redis.host }}
- name: PEERTUBE_REDIS_HOSTNAME
  value: {{.Values.redis.host | quote}}
- name: PEERTUBE_REDIS_PORT
  value: {{.Values.redis.port | quote}}
- name: PEERTUBE_REDIS_AUTH
  value: {{.Values.redis.auth | quote}}
{{- end }}

{{- if eq .Values.objectStorage.status "enabled" }}
- name: PEERTUBE_OBJECT_STORAGE_ENABLED
  value: "true"
- name: PEERTUBE_OBJECT_STORAGE_ENDPOINT
  value: {{.Values.objectStorage.endpoint | quote }}
- name: PEERTUBE_OBJECT_STORAGE_REGION
  value: {{.Values.objectStorage.region | quote }}
- name: PEERTUBE_OBJECT_STORAGE_UPLOAD_ACL_PUBLIC
  value: {{.Values.objectStorage.uploadACLPublic | quote }}
- name: PEERTUBE_OBJECT_STORAGE_UPLOAD_ACL_PRIVATE
  value: {{.Values.objectStorage.uploadACLPrivate | quote }}
- name: PEERTUBE_OBJECT_STORAGE_PROXY_PROXIFY_PRIVATE_FILES
  value: {{.Values.objectStorage.proxifyPrivateFiles | quote }}
- name: PEERTUBE_OBJECT_STORAGE_CREDENTIALS_ACCESS_KEY_ID
  value: {{.Values.objectStorage.accessKey | quote }}
- name: PEERTUBE_OBJECT_STORAGE_CREDENTIALS_SECRET_ACCESS_KEY
  value: {{.Values.objectStorage.secretKey | quote }}
- name: PEERTUBE_OBJECT_STORAGE_MAX_UPLOAD_PART
  value: {{.Values.objectStorage.maxUploadPart | quote }}
- name: PEERTUBE_OBJECT_STORAGE_STREAMING_PLAYLISTS_BUCKET_NAME
  value: {{.Values.objectStorage.streaming.playlists.bucket | quote }}
- name: PEERTUBE_OBJECT_STORAGE_STREAMING_PLAYLISTS_PREFIX
  value: {{.Values.objectStorage.streaming.playlists.prefix | quote }}
- name: PEERTUBE_OBJECT_STORAGE_STREAMING_PLAYLISTS_BASE_URL
  value: {{.Values.objectStorage.streaming.playlists.baseUrl | quote }}
- name: PEERTUBE_OBJECT_STORAGE_UPLOAD_ACL
  value: {{.Values.objectStorage.uploadACL | quote }}
- name: PEERTUBE_OBJECT_STORAGE_VIDEOS_BUCKET_NAME
  value: {{.Values.objectStorage.videos.bucket | quote }}
- name: PEERTUBE_OBJECT_STORAGE_VIDEOS_PREFIX
  value: {{.Values.objectStorage.videos.prefix | quote }}
- name: PEERTUBE_OBJECT_STORAGE_VIDEOS_BASE_URL
  value: {{.Values.objectStorage.videos.baseUrl | quote }}
{{- end -}}

{{- if .Values.smtp.host }}
- name: PEERTUBE_SMTP_USERNAME
  value: {{.Values.smtp.username | quote}}
- name: PEERTUBE_SMTP_PASSWORD
  value: {{.Values.smtp.password | quote}}
- name: PEERTUBE_SMTP_HOSTNAME
  value: {{.Values.smtp.host | quote}}
- name: PEERTUBE_SMTP_PORT
  value: {{.Values.smtp.port | quote}}
- name: PEERTUBE_SMTP_FROM
  value: {{.Values.smtp.from | quote}}
- name: PEERTUBE_SMTP_TLS
  value: {{.Values.smtp.tls | quote}}
- name: PEERTUBE_SMTP_DISABLE_STARTTLS
  value: {{ not .Values.smtp.startTLS | quote}}
{{- end -}}
{{- end -}}

{{- define "s3fs.commonEnv" -}}
- name: AWS_S3_MOUNT
  value: {{.Values.objectStorage.s3fs.mountPath | quote }}
- name: AWS_S3_BUCKET
  value: {{.Values.objectStorage.bucket | quote}}
- name: AWS_S3_ACCESS_KEY_ID
  value: {{.Values.objectStorage.accessKey | quote}}
- name: AWS_S3_SECRET_ACCESS_KEY
  value: {{.Values.objectStorage.secretKey | quote}}
- name: AWS_S3_URL
  value: {{.Values.objectStorage.endpoint | quote }}
- name: S3FS_ARGS
  value: {{.Values.objectStorage.s3fs.args | quote }}
{{- if .Values.objectStorage.s3fs.uid }}
- name: UID
  value: {{.Values.objectStorage.s3fs.uid | quote }}
{{- end }}
{{- if .Values.objectStorage.s3fs.gid }}
- name: GID
  value: {{.Values.objectStorage.s3fs.gid | quote }}
{{- end -}}
{{- end -}}
